import numpy as np
import time
import h5py
import warnings
with warnings.catch_warnings():
    warnings.simplefilter("ignore")
    import pyjapc
try:
    from tqdm.autonotebook import tqdm
except ImportError:
    tqdm = None
import copy

import sys
# Add the ptdraft folder path to the sys.path list
sys.path.append('./Awake-Traj-Corr')

from corrTraj import CorrectTrajectory


    
class ctlDevice():
    def __init__(self, setName, acqName="", epsilon = 1):
        self.setName = setName
        self.acqName = acqName
        self.tol = epsilon
        
        
class groupQuery():
    #groupName is used to generate .h5 file
    #paraDict is made up of {#paraname: <a device object>}

    def __init__(self, acqList, setDict, groupName=""):
        self.h5name = groupName
        self.japc = pyjapc.PyJapc("SPS.USER.ALL")
        self.CT = CorrectTrajectory(self.japc)
        self.CT.cal_file = "./Awake-Traj-Corr/booster_calibration_1565450974.h5"
        self.CT.loadCalibration()
        self.CT.loadBooster()
        
        self.shotCount = 1
        self.acqList = acqList
        self.groupName = groupName
        self.setDict = setDict
        self.lastAcq = time.time()
        self.t_string = str(round(time.time()))
        self.result = {x:[] for x in self.acqList}
        self.BPMs = [
        "TT43.BPM.430010/LastAcq#verPos",
        "TT43.BPM.430010/LastAcq#horPos",
        "TT43.BPM.430010/LastAcq#sigma",
        "TT43.BPM.430028/LastAcq#verPos",
        "TT43.BPM.430028/LastAcq#horPos",
        "TT43.BPM.430028/LastAcq#sigma",
        "TT43.BPM.430039/LastAcq#verPos",
        "TT43.BPM.430039/LastAcq#horPos",
        "TT43.BPM.430039/LastAcq#sigma",
        ]
        self.BPM_vals = {x:[] for x in self.BPMs}
        
    def prepare_BPMs(self):
        for eachBPM in self.BPMs:
            self.japc.subscribeParam(eachBPM, self.cb_BPM, timingSelectorOverride='SPS.USER.ALL')
        
        
    def clear_results(self):
        self.result = {x:[] for x in self.acqList}
        
    def cb_BPM(self, devNames, value):
        self.BPM_vals[devNames].append(copy.deepcopy(value))
    
    #both are lists
    def callbacks(self, devNames, values):
        holder = 0
        for _ in tqdm(range(self.shotCount)):
            for idx, v in enumerate(values):
                self.result[self.acqList[idx]].append(v)
            #have to be a blocking wait, stupid japc
            t = time.time()
        while time.time()-t < 0.1:
            holder+=1
        holder = 0
               
        self.stopQuery()
        self.not_done = False
      
    def __getitem__(self, key):
        return self.paraDict[key]
        
    def ls(self):
        print(self.paraDict)
    
    
    # thanks Spencer, fucking magnets!
    def computeCurrentFromK(self, k_val):
        eMom = self.getValue('rmi://virtual_sps/TT43BEAM/MOMENTUM')
        qBDL = 0.19272/0.0718
        c_unit = 0.2998
        max_current = 10
        quad_constant =  (eMom * max_current)/(qBDL * c_unit)
        return (quad_constant * k_val)
    
    """set_and_wait(self, deviceName, targetValue, epsilon = 1)"""
    def set_and_wait(self, deviceName, targetValue, epsilon = 0.1):
        if deviceName == "Corrector":
            device = ctlDevice("RPCAH.TSG4.RCIBV.430029/SettingPPM#current", 
                               acqName="RPCAH.TSG4.RCIBV.430029/Acquisition#currentAverage")
        else:
            device = self.setDict[deviceName]
        self.japc.setParam(device.setName,targetValue,timingSelectorOverride='')
        
        if device.acqName != "":
            readBack = self.japc.getParam(device.acqName,timingSelectorOverride='')
            # the read back for setting K is current not K
            if device.setName == 'rmi://virtual_sps/logical.RQIF.430034/K':
                t = self.computeCurrentFromK(targetValue)
            else:
                t = targetValue
            time_out = time.time()
            while np.abs(readBack - t) > epsilon:
                readBack = self.japc.getParam(device.acqName,timingSelectorOverride='')
                if time.time() - time_out > 10:
                    warnings.warn("{} won't stable arount {} +/- {}".format(deviceName, 
                                                                            targetValue, 
                                                                            epsilon), RuntimeWarning)
                    print(RuntimeError("{} won't stable around {}".format(deviceName, t)))
                time.sleep(0.2)
            print("{} is stable {} +/- {}".format(deviceName,t, epsilon))
        else:
            print("{} is set to {}".format(deviceName, t))
        
    def getValue(self, deviceName):
        temp = self.japc.getParam(deviceName, timingSelectorOverride='')
        return temp
    
    def getResult(self, nShots=1):
        self.shotCount = nShots
        self.clear_results()
        self.t_string = str(round(time.time()))
        self.japc.subscribeParam(self.acqList, self.callbacks, timingSelectorOverride='')
        self.not_done = True
        self.japc.startSubscriptions()
        while self.not_done:
            time.sleep(1)
        return self.result
    
    # get a {key: val} list for all the the devices in acqList
    def getAll(self):
        self.clear_results()
        self.t_string = str(round(time.time()))
        time.sleep(0.1)
        return dict(zip(self.acqList, self.japc.getParam(self.acqList, timingSelectorOverride='')))
    
    #scanDict contains name:list for each name, a list of target values is given
    def scantoH5(self, scanDict, squashShots = False, nShots=10, burn_in = 0, dry_run=False, outdir="./", record_BPM=False):
        t_string = str(round(time.time()))
        acqList = self.acqList
        setList = list(scanDict.keys())
        paraLen = len(scanDict[setList[0]])
        if record_BPM:    
            datas = {x:[] for x in acqList+self.BPMs}
#             self.prepare_BPMs()
        else:
            datas = {x:[] for x in acqList}
        #current_para = {x:0 for x in setList}
        #paras = {x:[] for x in setList}
        #initialize the AWAKE to desired starting parameter
        for i, (key, valList) in enumerate(scanDict.items()):
            if not dry_run:
                try:
                    self.set_and_wait(key, valList[0])
                except RuntimeError:
                    warnings.warn("Retrying", RuntimeWarning)
                    self.set_and_wait(key, valList[0])
        print("About to get initial trajectory")
        self.stopQuery()
        self.set_and_wait("Corrector", 0)
        self.CT.getTrajectory()
        self.set_and_wait("Corrector", 2)
        print("Initial Trajectory is acquired")
        if np.any(np.abs(self.CT.delta_x) > 0.2):
            self.CT.applyCorrection()
            #current_para[key] = valList[0]
        if (tqdm == None):
            pbar = None
        else:
            pbar = tqdm(total=paraLen, desc="Total points")
        
        self.lastSole = scanDict["Solenoid"][0]
        
        for i in range(paraLen):
            # align all set value to i's in their list
            for setName in setList:
                val = scanDict[setName][i]
                #current_para[setName] = val
                if not dry_run:
                    try:
                        self.set_and_wait(setName, val)
                        if setName == "Solenoid" and self.lastSole != val:
                            self.lastSole = val
                            self.stopQuery()
                            self.set_and_wait("Quad", 0)
                            self.set_and_wait("Corrector", 0)
                            self.CT.getTrajectory()
                            self.set_and_wait("Corrector", 2)
                            if np.any(np.abs(self.CT.delta_x) > 0.2):
                                self.CT.applyCorrection()
                                print("Trajectory correction is applied")
                        self.set_and_wait(setName, val)
                    except RuntimeError:
                        warnings.warn("Retrying in 5 seconds and will continue without stable", RuntimeWarning)
                        time.sleep(5)
                        self.set_and_wait(setName, val)
            self.prepare_BPMs()
            self.japc.startSubscriptions()
            print("start my subscriptions")
            cols = [[copy.deepcopy(self.getAll()[name]) for _ in range(nShots)] for name in acqList]
            self.stopQuery()
            if (pbar):
                pbar.update(1)
            for (idx,val) in enumerate(acqList):
                if squashShots:
                    datas[val].append(np.mean(cols[idx], axis=0))
                else:
                    datas[val].append(cols[idx])
            if record_BPM:
                #self.japc.stopSubscriptions()
                for val in self.BPMs:
                    datas[val].append(copy.deepcopy(self.BPM_vals[val][-nShots:]))
#                 self.BPM_vals = {x:[] for x in self.BPMs}
#             for k in setList:
#                 paras[k].append(copy.deepcopy(k))
        pbar.close()
        self.stopQuery()
        try:
            if dry_run:
                f = h5py.File(outdir + "/" + 'test.h5','w')
            else:
                f = h5py.File(outdir + "/" + self.groupName + t_string + '.h5','w')
            for name in acqList:
                f.create_dataset(name[:name.find('/')], data=datas[name], compression="lzf")
            for name in self.BPMs:
                f.create_dataset(name, data=datas[name], compression="lzf")
            for i, (key, vals) in enumerate(scanDict.items()):
                f.create_dataset(key,data=vals)
            f.create_dataset("Specs", data = str({"scanParaLen": paraLen, "nShots" : nShots}))
            f.close()
            return None
        except:
            warnings.warn("Faild to save, here's the tuple, wish you the best")
            return (datas, paras, str({"scanParaLen": paraLen, "nShots" : nShots}))
    
            
    def stopQuery(self):
        self.japc.stopSubscriptions()
        self.japc.clearSubscriptions()
        